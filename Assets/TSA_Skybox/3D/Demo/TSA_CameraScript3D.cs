﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TSA_CameraScript3D : MonoBehaviour
{
    public float rotationSpeed = 60;

    void Update()
    {
        transform.Rotate(new Vector2(Input.GetAxis("Vertical"), -Input.GetAxis("Horizontal")) * rotationSpeed * Time.deltaTime);
    }
}
