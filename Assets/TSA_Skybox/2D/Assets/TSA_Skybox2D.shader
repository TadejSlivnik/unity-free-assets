﻿Shader "_TS/TSA/Skybox/2D"
{
	Properties
	{
        _Tint ("Tint Color", Color) = (.5, .5, .5, .5)
        [Gamma] _Exposure ("Exposure", Range(0, 8)) = 1.0
        [NoScaleOffset] _MainTex("Main Texture (HDR)", 2D) = "grey" {}
	}

	SubShader
	{
		Tags
		{
			"RenderType"="Background"
			"Queue"="Background"
            "ForceNoShadowCasting"="True"
			"IgnoreProjector"="True"
            "PreviewType"="Plane"
		}
		
        ZWrite Off

		Pass
		{
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			
			#include "UnityCG.cginc"

			sampler2D _MainTex;
			float4 _MainTex_ST;
			half4 _MainTex_HDR;

			half4 _Tint;
			half _Exposure;

			struct appdata
			{
				float4 vertex : POSITION;
				float2 uv : TEXCOORD0;
			};

			struct v2f
			{
				float4 vertex : SV_POSITION;
				float2 uv : TEXCOORD0;
			};
			
			v2f vert (appdata v)
			{
				v2f o;
				
				v.uv = v.uv - 0.5;
				o.vertex = fixed4(v.uv.x, -v.uv.y, 0, 0.5);
				o.uv = v.uv * _MainTex_ST.xy * 0.99 + _MainTex_ST.zw + 0.5;

				return o;
			}
			
			half4 frag (v2f i) : SV_Target
			{
				half3 c = DecodeHDR(tex2D(_MainTex, i.uv), _MainTex_HDR);
				c = c * _Tint.rgb * unity_ColorSpaceDouble.rgb;
				c *= _Exposure;
				return half4(c, 1);
			}
			ENDCG
		}
	}
}
