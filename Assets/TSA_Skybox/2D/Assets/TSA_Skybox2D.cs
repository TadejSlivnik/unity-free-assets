﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
[RequireComponent(typeof(MeshRenderer))]
[RequireComponent(typeof(MeshFilter))]
public class TSA_Skybox2D : MonoBehaviour
{
    public Texture2D texture;
    public float aspectRatio = 1.777778f; // 16/9

    private readonly string shader = "_TS/TSA/Skybox/2D";

    /// <summary>
    /// Updates aspect ratio on start.
    /// </summary>
    [Tooltip("Updates aspect ratio on start.")]
    public bool aspectOnStart = true;
    /// <summary>
    /// Updates aspect ration if needed.
    /// </summary>
    [Tooltip("Updates aspect ration if needed.")]
    public bool aspectOnUpdate = true;
    private float prevAspectDiv = -1;

    private void Awake()
    {
        if (GetComponent<MeshFilter>() == null)
            gameObject.AddComponent<MeshFilter>();
        if (GetComponent<MeshRenderer>() == null)
            gameObject.AddComponent<MeshRenderer>();
    }

    private void Start()
    {
        if (GetComponent<MeshFilter>().sharedMesh == null)
            CreateMesh();
        if (aspectOnStart)
            UpdateAspectRatio();
    }

    private void Update()
    {
        if (aspectOnUpdate)
            UpdateAspectRatio();
    }

    public void ApplyTexture(bool _updateAspectRatio) { ApplyTexture(_updateAspectRatio, texture); }
    public void ApplyTexture(bool _updateAspectRatio, Texture2D _texture)
    {
        texture = _texture;

        if (texture == null)
            return;

        MeshRenderer meshR = GetComponent<MeshRenderer>();
        meshR.sharedMaterial = new Material(Shader.Find(shader));
        meshR.sharedMaterial.SetTexture("_MainTex", texture);

        if (_updateAspectRatio)
            aspectRatio = _texture.width / (float)_texture.height;
    }
    
    public void UpdateAspectRatio()
    {
        float aspectDiv = Camera.main.aspect / aspectRatio;
        if (aspectDiv == prevAspectDiv)
            return;

        prevAspectDiv = aspectDiv;
        Vector2 textureScale;
        if (aspectDiv < 1)
            textureScale = new Vector2(aspectDiv, 1);
        else
            textureScale = new Vector2(1, 1f / aspectDiv);

        GetComponent<MeshRenderer>().sharedMaterial.SetTextureScale("_MainTex", textureScale);
    }

    /// <summary>
    /// On Start: gets called if mesh is null.
    /// </summary>
    public void CreateMesh()
    {
        int a = 1;

        Vector3[] vertices = new Vector3[]
        {
            new Vector3(-a, -a, 0),
            new Vector3(a, -a, 0),
            new Vector3(-a, a, 0),
            new Vector3(a, a, 0)
        };

        int[] triangles = new int[]
        {
            0, 2, 1,
            1, 2, 3
        };

        Vector2[] uvs = new Vector2[]
        {
            Vector2.zero,
            new Vector2(1, 0),
            new Vector2(0, 1),
            Vector2.one
        };

        Mesh mesh = new Mesh
        {
            name = "Skybox 2D",
            vertices = vertices,
            triangles = triangles,
            uv = uvs
        };
        
        mesh.bounds = new Bounds(Vector3.zero, Vector3.one * (float.MaxValue * 0.00000000000000000001f));
        GetComponent<MeshFilter>().sharedMesh = mesh;
    }
}
