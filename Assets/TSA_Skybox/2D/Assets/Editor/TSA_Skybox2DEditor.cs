﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(TSA_Skybox2D))]
public class TSA_Skybox2DEditor : Editor
{
    SerializedProperty texture;
    SerializedProperty aspectRatio;

    SerializedProperty aspectOnStart;
    SerializedProperty aspectOnUpdate;

    private TSA_Skybox2D skybox;

    private readonly GUIContent applyTexGUIContent = new GUIContent("Apply Texture", "Also calculates texture aspect ratio.");
    private readonly GUIContent meshGUIContent = new GUIContent("Create Mesh", "On Start: Mesh gets created automatically if it is null.");
    private readonly GUIContent aspectGUIContent = new GUIContent("Aspect On Start", "Updates aspect ratio on start.");

    private void OnEnable()
    {
        if (target == null)
        {
            DestroyImmediate(this);
            return;
        }

        texture = serializedObject.FindProperty("texture");
        aspectRatio = serializedObject.FindProperty("aspectRatio");
        aspectOnStart = serializedObject.FindProperty("aspectOnStart");
        aspectOnUpdate = serializedObject.FindProperty("aspectOnUpdate");

        skybox = (TSA_Skybox2D)target;
    }

    public override void OnInspectorGUI()
    {
        serializedObject.Update();

        if (GUILayout.Button(meshGUIContent, EditorStyles.miniButton))
            skybox.CreateMesh();
        if (GUILayout.Button(applyTexGUIContent))
            skybox.ApplyTexture(true);

        EditorGUILayout.Space();

        EditorGUILayout.PropertyField(texture);
        EditorGUILayout.Space();

        EditorGUILayout.PropertyField(aspectRatio);
        EditorGUILayout.BeginHorizontal();
        {
            EditorGUILayout.PrefixLabel(aspectGUIContent);
            EditorGUILayout.PropertyField(aspectOnStart, GUIContent.none, GUILayout.Width(20));
            if (GUILayout.Button("Apply Ratio", EditorStyles.miniButton))
                skybox.UpdateAspectRatio();
        }
        EditorGUILayout.EndHorizontal();
        EditorGUILayout.PropertyField(aspectOnUpdate);

        serializedObject.ApplyModifiedProperties();
    }
}
