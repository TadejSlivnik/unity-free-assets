﻿Shader "_TS/TSA/Grid 2D"
{
	Properties
	{
		_Color("Color", Color) = (1,0.5,0,1)
		[Header(Grid)]
		_GridOffset("Offset", Range(0.0, 1.0)) = 0.0
		_GridWidth("Width", Range(0.0001, 1.0)) = 0.1
		_GridScale("Scale", Float) = 1
		[Header(Highlight)]
		_Radius("Radius", Range(0.0, 1.0)) = 0.25
		_MouseWorldPos("Mouse World Position", Vector) = (0,0,0,0)
	}

	SubShader
	{
		Tags
		{
            "RenderType"="Transparent"
            "Queue"="Transparent"
			"PreviewType"="Plane"
		}
		Zwrite Off

		Pass
		{
			Blend SrcAlpha One
			CGPROGRAM
			
			#pragma vertex vert
			#pragma fragment frag
			#include "UnityCG.cginc"

			fixed4 _Color;
			fixed _GridWidth;
			float _GridScale;
			fixed _GridOffset;
			fixed _Radius;
			float4 _MouseWorldPos;

			struct appdata
			{
				float4 vertex : POSITION;
			};

			struct v2f
			{
				float4 pos : SV_POSITION;
				float4 worldPos : TEXCOORD0;
			};

			v2f vert(appdata v)
			{
				v2f o;
				
				o.pos = UnityObjectToClipPos(v.vertex);
				o.worldPos = mul(unity_ObjectToWorld, v.vertex);

				return o;
			}

			fixed4 frag (v2f i) : SV_Target
			{
				float2 vect = _MouseWorldPos.zw - i.worldPos;
				float radius = vect.x * vect.x + vect.y * vect.y;

				float2 pos = abs(i.worldPos - _GridOffset) % _GridScale;
				pos = abs(pos - _GridScale * 0.5) * 2;
				pos = saturate(pos - (1-_GridWidth) - (_GridScale - 1)) / _GridWidth;
				pos.x = max(pos.x, pos.y);
				
				fixed4 color = _Color;
				color.w = pos.x * pos.x;
				color.w *= color.w;
				color.w *= saturate(1/(radius * _Radius + 1));

				return color;
			}

			ENDCG
		}
	}
	Fallback "Unlit/Texture"
}