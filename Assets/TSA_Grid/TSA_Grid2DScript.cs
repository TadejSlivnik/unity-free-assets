﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(MeshRenderer))]
public class TSA_Grid2DScript : MonoBehaviour
{
    private Material material;

    private void Awake()
    {
        material = GetComponent<MeshRenderer>().material;
    }

    private void LateUpdate()
    {
        Vector2 mousePosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        Vector2Int mousePositionInt = Vector2Int.FloorToInt(mousePosition); // this is not necessary... maybe useful in the future
        material.SetVector("_MouseWorldPos", new Vector4(mousePositionInt.x, mousePositionInt.y, mousePosition.x, mousePosition.y));
        //material.SetFloat("_Width", Camera.main.orthographicSize * 0.005f);
        //material.SetFloat("_Radius", 5f / Camera.main.orthographicSize);
    }
}
