﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TSA_Shield3DNode
{
    public Vector3 pos = Vector3.zero;

    private float time;
    public float UpdateTime(float deltaT)
    {
        time += deltaT;
        return time;
    }

    public void Reset()
    {
        time = 0;
        active = true;
    }

    public bool active;
    public bool IsAnimating { get { return time < 1; } }
}
