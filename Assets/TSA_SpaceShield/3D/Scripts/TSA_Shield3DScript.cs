﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class TSA_Shield3DScript : MonoBehaviour
{
    public bool randomImpact;
    public float randomImpactCD = 0.1f;
    [Range(0.01f, 2)]
    public float impactForce = 0.5f;
    private float randomImpactTime;

    private List<TSA_Shield3DNode> nodes = new List<TSA_Shield3DNode>();
    private MeshRenderer meshRend;
    private const int maxPositions = 16;

    void Start()
    {
        meshRend = GetComponent<MeshRenderer>();
    }

    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            RaycastHit hit;
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            if (Physics.Raycast(ray, out hit))
                if (hit.collider.gameObject == gameObject)
                    Add(hit.point, impactForce);
        }

        if (randomImpact)
        {
            if (Time.time >= randomImpactTime)
            {
                randomImpactTime = Time.time + randomImpactCD;
                Add(transform.position + Random.insideUnitSphere * 2, impactForce);
            }
        }

        for (int i = 0; i < nodes.Count; i++)
        {
            if (nodes[i].active)
            {
                if (nodes[i].IsAnimating)
                {
                    meshRend.material.SetVector("_Position" + i.ToString(), 
                        new Vector4(nodes[i].pos.x, nodes[i].pos.y, nodes[i].pos.z, Mathf.Clamp01(nodes[i].UpdateTime(Time.deltaTime)))
                        );
                }
                else
                {
                    nodes[i].active = false;
                    meshRend.material.SetVector("_Position" + i.ToString(), new Vector4(nodes[i].pos.x, nodes[i].pos.y, nodes[i].pos.z, 0));
                }
            }
        }
    }

    public void Add(Vector3 _impactPosition, float _intensity)
    {
        TSA_Shield3DNode node = null;
        if (nodes.Count < maxPositions)
        {
            node = new TSA_Shield3DNode();
            nodes.Add(node);
        }
        else
            node = nodes.Find(x => x.active == false);

        if (node != null)
        {
            node.pos = (_impactPosition - transform.position).normalized;
            SetIntensity(nodes.IndexOf(node), _intensity);
            node.Reset();
        }
    }

    private void SetIntensity(int _nodeIndex, float _intensity)
    {
        _intensity = Mathf.Abs(_intensity) / transform.lossyScale.x;
        Vector4 intensityVector;
        string propName;
        if (_nodeIndex < 4)
            propName = "_Radius0";
        else if (_nodeIndex < 8)
            propName = "_Radius1";
        else if (_nodeIndex < 12)
            propName = "_Radius2";
        else
            propName = "_Radius3";

        intensityVector = meshRend.material.GetVector(propName);
        if (_nodeIndex % 4 == 0)
            intensityVector.x = _intensity;
        else if (_nodeIndex % 4 == 1)
            intensityVector.y = _intensity;
        else if (_nodeIndex % 4 == 2)
            intensityVector.z = _intensity;
        else
            intensityVector.w = _intensity;
        meshRend.material.SetVector(propName, intensityVector);
    }
}
