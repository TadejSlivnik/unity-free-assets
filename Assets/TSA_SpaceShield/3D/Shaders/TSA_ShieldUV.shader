﻿Shader "_TS/TSA/Shield/UV - Cube, Square, Circle"
{
	Properties
	{
		[IntRange]_Cull("Cull", RAnge(0, 2)) = 1
		[KeywordEnum(Normal, Radial)] _UVMode("UV Mode", Float) = 1
		[KeywordEnum(Plain, Custom)]_Coloring("Coloring", Float) = 1
		// [Header(Texture)]
		_MainTex ("Main Texture", 2D) = "black" {}
		// [Header(Rim)]
		_RimColor("Rim Color", Color) = (1, 1, 1, 1)
		_RimPower("Rim Power", Range(0.5, 4)) = 2
		_RimOpacity("Rim Opacity", Range(0.0, 1.0)) = 1.0
	}
	SubShader
	{
		Blend SrcAlpha One
		Tags 
		{
			"RenderType"="Transparent"
			"Queue"="Transparent"
			"PreviewType" = "Plane"
		}
		ZWrite Off
		Lighting Off
		Cull [_Cull]

		Pass
		{
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#pragma multi_compile _UVMODE_NORMAL _UVMODE_RADIAL
			#pragma shader_feature _COLORING_PLAIN _COLORING_CUSTOM
			
			#include "UnityCG.cginc"
			
			sampler2D _MainTex;
			float4 _MainTex_ST;
			
			half _Cull;

			fixed4 _RimColor;
			float _RimPower;
			fixed _RimOpacity;

			struct appdata
			{
				float4 vertex : POSITION;
				float2 uv : TEXCOORD0;
			};

			struct v2f
			{
				float4 vertex : SV_POSITION;
				float4 uv : TEXCOORD0;
			};
			
			v2f vert (appdata v)
			{
				v2f o;

				o.vertex = UnityObjectToClipPos(v.vertex);
				o.uv.xy = v.uv * 2 - 1;
				o.uv.zw = TRANSFORM_TEX(v.uv, _MainTex);

				return o;
			}
			
			fixed4 frag (v2f i) : SV_Target
			{
				fixed2 radius = 1;
				#ifdef _UVMODE_NORMAL
					i.uv.xy = abs(i.uv.xy);
					radius.x = max(i.uv.x, i.uv.y);
				#else
					radius.x = sqrt(i.uv.x * i.uv.x + i.uv.y * i.uv.y);
				#endif

				#ifdef _UVMODE_RADIAL
					radius.x *= saturate(ceil(1 - radius.x));
				#endif

				radius.x *= radius.x;
				radius.x *= radius.x;
				radius.x = pow(radius.x, _RimPower);
				
				// Rim
				#ifdef _COLORING_CUSTOM
					radius.y = radius.x * 0.75;
					radius.y *= radius.y;
					fixed4 rimCol = lerp(0, lerp(_RimColor, 1, radius.y), radius.x);
				#else
					fixed4 rimCol = _RimColor * radius.x;
				#endif
				rimCol *= _RimOpacity;

				#ifdef _IMPACTTYPE_WAVE
					rimCol += tex2D(_MainTex, i.uv.zw) * radius.x * (1 - radius.x);
				#else
					rimCol += tex2D(_MainTex, i.uv.zw) * radius.x * (1 - radius.x);
				#endif

				return rimCol;
			}
			ENDCG
		}
	}
    CustomEditor "PPS_ShieldUVShaderGUI"
}
