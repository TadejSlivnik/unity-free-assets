﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class TSA_ShieldUVShaderGUI : TSA_AShieldShaderGUI
{
    public override void OnGUI(MaterialEditor _editor, MaterialProperty[] _properties)
    {
        Material targetMat = _editor.target as Material;

        GUILayout.Label("Settings", EditorStyles.boldLabel);
        ShowCullEnum(targetMat, _editor);
        ShowUVEnum(targetMat, _editor);
        ShowColoringEnum(targetMat, _editor);
        DoSpaceShieldArea(_editor, _properties);
    }

    protected override void DoSpaceShieldArea(MaterialEditor _editor, MaterialProperty[] _properties)
    {
        GUILayout.Label("Texture", EditorStyles.boldLabel);
        ShowTextureSingleLine(_editor, _properties, "Main Texture", "_MainTex", "Pattern.");
        base.DoSpaceShieldArea(_editor, _properties);
    }
}
