﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class TSA_Shield3DFXShaderGUI : TSA_AShieldShaderGUI
{
    public override void OnGUI(MaterialEditor _editor, MaterialProperty[] _properties)
    {
        Material targetMat = _editor.target as Material;

        GUILayout.Label("Settings", EditorStyles.boldLabel);
        ShowCullEnum(targetMat, _editor);
        ShowImpactTypeEnum(targetMat, _editor);
        ShowImpactyIntensityEnum(targetMat, _editor);
        ShowColoringEnum(targetMat, _editor);

        bool toggle = false;
        ShowToggle(targetMat, _editor, out toggle, "UV Pattern", "_UVPATTERN_ON", "Needs a icosahedron based sphere with custom UVs.");
        if (toggle)
        {
            GUILayout.Label(new GUIContent("UV Pattern", "Needs a icosahedron based sphere with custom UVs."), EditorStyles.boldLabel);
            ShowShaderProperty(_editor, _properties, "UV Power", "_UVPower", "");
            ShowShaderProperty(_editor, _properties, "UV Opacity", "_UVOpacity", "");
        }

        DoSpaceShieldArea(_editor, _properties);
    }
}
