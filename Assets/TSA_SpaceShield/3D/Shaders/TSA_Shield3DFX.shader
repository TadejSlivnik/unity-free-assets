﻿Shader "_TS/TSA/Shield/3D FX - Sphere"
{
	Properties 
	{
		[IntRange]_Cull("Cull", RAnge(0, 2)) = 1
		[KeywordEnum(Wave, Simple)]_ImpactType("Impact Type", Float) = 0
		[KeywordEnum(Max, Add)]_ImpactIntensity("Impact Intensity", Float) = 0
		[KeywordEnum(Plain, Custom)]_Coloring("Coloring", Float) = 1
		[Toggle]_UVPattern("UV Pattern", Float) = 1
		// [Header(UV Pattern)]
		_UVPower("UV Power", Range(1, 20)) = 16
		_UVOpacity("UV Opacity", Range(0, 1)) = 0.333
		// [Header(Rim)]
		_RimColor("Rim Color", Color) = (1, 1, 1, 1)
		_RimPower("Rim Power", Range(0.5, 4)) = 2
		_RimOpacity("Rim Opacity", Range(0.0, 1.0)) = 1.0
		[HideInInspector]_Radius0("Radius 0", Vector) = (1,1,1,1)
		[HideInInspector]_Radius1("Radius 1", Vector) = (1,1,1,1)
		[HideInInspector]_Radius2("Radius 2", Vector) = (1,1,1,1)
		[HideInInspector]_Radius3("Radius 3", Vector) = (1,1,1,1)
	}
	SubShader
	{
		Blend SrcAlpha One
		Tags
		{ 
			"RenderType"="Transparent"
			"Queue"="Transparent"
		}
		ZWrite Off
		Lighting Off
		Cull [_Cull]

		Pass 
		{

			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#pragma target 2.0

			#pragma shader_feature _IMPACTTYPE_WAVE _IMPACTTYPE_SIMPLE
			#pragma shader_feature _IMPACTINTENSITY_MAX _IMPACTINTENSITY_ADD
			#pragma shader_feature _COLORING_PLAIN _COLORING_CUSTOM
			#pragma shader_feature _UVPATTERN_OFF _UVPATTERN_ON

			#include "UnityCG.cginc"

			#ifdef _UVPATTERN_ON
				float _UVPower;
				fixed _UVOpacity;
			#endif

			fixed4 _RimColor;
			float _RimPower;
			fixed _RimOpacity;

			half _Cull;
			
			float4 _Radius0, _Radius1, _Radius2, _Radius3;
			float4 _Position0, _Position1, _Position2, _Position3;
			float4 _Position4, _Position5, _Position6, _Position7;
			float4 _Position8, _Position9, _Position10, _Position11;
			float4 _Position12, _Position13, _Position14, _Position15;

			struct appdata
			{
				float4 vertex : POSITION;
				float3 normal : NORMAL;
				#ifdef _UVPATTERN_ON
					float2 uv : TEXCOORD0;
				#endif
			};

			struct v2f
			{
				float4 vertex : SV_POSITION;
				fixed3 normal : NORMAL;
				fixed3 pos : TEXCOORD0;
				fixed3 viewDir : TEXCOORD1;
				#ifdef _UVPATTERN_ON
					float2 uv : TEXCOORD2;
				#endif
			};

			v2f vert(appdata v)
			{
				v2f o;

				o.vertex = UnityObjectToClipPos(v.vertex);
				o.normal = normalize(mul((float3x3)unity_ObjectToWorld, v.normal));
				o.pos = normalize(v.normal.xyz);
				#ifdef _UVPATTERN_ON
					o.uv = 1 - v.uv;
				#endif
				
				float3 worldPos = mul(unity_ObjectToWorld, v.vertex);
				o.viewDir = normalize(_WorldSpaceCameraPos - worldPos);

				return o;
			}
			
			fixed4 calcIntensity(float3 _vertexPos, float4 _impactPos, float _radius) 
			{
				float impactRadius = _impactPos.w * _radius;
				float dist = length(_impactPos.xyz - _vertexPos);

				#ifdef _IMPACTTYPE_WAVE
					float distFromRad = saturate(dist - impactRadius) / max(dist, saturate(_impactPos.w));
					fixed distFactor = (1 - sin(distFromRad)) * cos(distFromRad);
					distFactor *= distFactor;
					distFactor *= distFactor;
					distFactor *= distFactor;
					distFactor += impactRadius / (dist + _radius * 0.333) * 0.75;
				#else
					fixed distFactor = impactRadius / (dist + _radius * 0.2);
				#endif
				
				distFactor = saturate(distFactor * (1 - _impactPos.w));
				
				#ifdef _COLORING_CUSTOM
					return lerp(0, lerp(_RimColor, 1, distFactor * distFactor), distFactor);
				#else
					return distFactor * _RimColor;
				#endif
			}

			fixed4 frag(v2f i) : SV_Target
			{
				fixed4 intensity = 0;

				#ifdef _IMPACTINTENSITY_MAX
					intensity = max(intensity, calcIntensity(i.pos, _Position0, _Radius0.x));
					intensity = max(intensity, calcIntensity(i.pos, _Position1, _Radius0.y));
					intensity = max(intensity, calcIntensity(i.pos, _Position2, _Radius0.z));
					intensity = max(intensity, calcIntensity(i.pos, _Position3, _Radius0.w));
					intensity = max(intensity, calcIntensity(i.pos, _Position4, _Radius1.x));
					intensity = max(intensity, calcIntensity(i.pos, _Position5, _Radius1.y));
					intensity = max(intensity, calcIntensity(i.pos, _Position6, _Radius1.z));
					intensity = max(intensity, calcIntensity(i.pos, _Position7, _Radius1.w));
					intensity = max(intensity, calcIntensity(i.pos, _Position8, _Radius2.x));
					intensity = max(intensity, calcIntensity(i.pos, _Position9, _Radius2.y));
					intensity = max(intensity, calcIntensity(i.pos, _Position10, _Radius2.z));
					intensity = max(intensity, calcIntensity(i.pos, _Position11, _Radius2.w));
					intensity = max(intensity, calcIntensity(i.pos, _Position12, _Radius3.x));
					intensity = max(intensity, calcIntensity(i.pos, _Position13, _Radius3.y));
					intensity = max(intensity, calcIntensity(i.pos, _Position14, _Radius3.z));
					intensity = max(intensity, calcIntensity(i.pos, _Position15, _Radius3.w));
				#else
					intensity += calcIntensity(i.pos, _Position0, _Radius0.x);
					intensity += calcIntensity(i.pos, _Position1, _Radius0.y);
					intensity += calcIntensity(i.pos, _Position2, _Radius0.z);
					intensity += calcIntensity(i.pos, _Position3, _Radius0.w);
					intensity += calcIntensity(i.pos, _Position4, _Radius1.x);
					intensity += calcIntensity(i.pos, _Position5, _Radius1.y);
					intensity += calcIntensity(i.pos, _Position6, _Radius1.z);
					intensity += calcIntensity(i.pos, _Position7, _Radius1.w);
					intensity += calcIntensity(i.pos, _Position8, _Radius2.x);
					intensity += calcIntensity(i.pos, _Position9, _Radius2.y);
					intensity += calcIntensity(i.pos, _Position10, _Radius2.z);
					intensity += calcIntensity(i.pos, _Position11, _Radius2.w);
					intensity += calcIntensity(i.pos, _Position12, _Radius3.x);
					intensity += calcIntensity(i.pos, _Position13, _Radius3.y);
					intensity += calcIntensity(i.pos, _Position14, _Radius3.z);
					intensity += calcIntensity(i.pos, _Position15, _Radius3.w);
				#endif
				
				// Rim
				fixed VdotN = 1 - abs(dot(i.viewDir, i.normal));
				fixed4 rimCol = 0.75 + 0.25 * min(_Cull, 1);
				#ifdef _UVPATTERN_ON
					rimCol += pow(i.uv.x, _UVPower)	* _UVOpacity;
				#endif
				#ifdef _IMPACTTYPE_WAVE
					rimCol *= max(pow(VdotN, _RimPower), intensity * intensity);
				#else
					rimCol *= max(pow(VdotN, _RimPower), intensity);
				#endif

				#ifdef _COLORING_CUSTOM
					rimCol = lerp(0, 1.2 * lerp(_RimColor, 1, rimCol * rimCol), rimCol);
				#else
					rimCol *= _RimColor;
				#endif
				rimCol *= _RimOpacity;


				return intensity + rimCol;
			}
			ENDCG
		}
	}
    CustomEditor "PPS_Shield3DFXShaderGUI"
}
