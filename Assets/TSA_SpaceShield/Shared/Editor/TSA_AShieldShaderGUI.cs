﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class TSA_AShieldShaderGUI : PPS_AShaderGUI
{
    private enum CullMode { Off, Front, Back }
    private CullMode CurrentCullType(Material _targetMat)
    {
        CullMode cullMode = CullMode.Back;
        if (_targetMat.GetFloat("_Cull") == 0)
            cullMode = CullMode.Off;
        else if (_targetMat.GetFloat("_Cull") == 1)
            cullMode = CullMode.Front;

        return cullMode;
    }

    protected void ShowCullEnum(Material _targetMat, MaterialEditor _editor)
    {
        CullMode cullMode = CurrentCullType(_targetMat);
        EditorGUI.BeginChangeCheck();
        cullMode = (CullMode)EditorGUILayout.EnumPopup(new GUIContent("Cull Mode", "_CullMode - Float"), cullMode);
        if (EditorGUI.EndChangeCheck())
        {
            _editor.RegisterPropertyChangeUndo("UV Mode");
            _targetMat.SetFloat("_Cull", (int)cullMode);
        }
    }

    private enum UVMode { Normal, Radial }
    private UVMode CurrentLightingType(Material _targetMat)
    {
        UVMode uvMode = UVMode.Normal;
        if (_targetMat.IsKeywordEnabled("_UVMODE_RADIAL"))
            uvMode = UVMode.Radial;

        return uvMode;
    }

    protected void ShowUVEnum(Material _targetMat, MaterialEditor _editor)
    {
        UVMode uvMode = CurrentLightingType(_targetMat);
        EditorGUI.BeginChangeCheck();
        uvMode = (UVMode)EditorGUILayout.EnumPopup(new GUIContent("UV Mode", "_UVMode - Multi Compile"), uvMode);
        if (EditorGUI.EndChangeCheck())
        {
            _editor.RegisterPropertyChangeUndo("UV Mode");
            SetKeyword(_targetMat, "_UVMODE_NORMAL", uvMode == UVMode.Normal);
            SetKeyword(_targetMat, "_UVMODE_RADIAL", uvMode == UVMode.Radial);
        }
    }

    private enum ImpactType { Wave, Simple }
    private ImpactType CurrentImpactType(Material _targetMat)
    {
        ImpactType impactType = ImpactType.Wave;
        if (_targetMat.IsKeywordEnabled("_IMPACTTYPE_SIMPLE"))
            impactType = ImpactType.Simple;

        return impactType;
    }

    protected void ShowImpactTypeEnum(Material _targetMat, MaterialEditor _editor)
    {
        ImpactType impactType = CurrentImpactType(_targetMat);
        EditorGUI.BeginChangeCheck();
        impactType = (ImpactType)EditorGUILayout.EnumPopup(new GUIContent("Impact Type", "_ImpactType - Shader Feature"), impactType);
        if (EditorGUI.EndChangeCheck())
        {
            _editor.RegisterPropertyChangeUndo("Impact Type");
            SetKeyword(_targetMat, "_IMPACTTYPE_WAVE", impactType == ImpactType.Wave);
            SetKeyword(_targetMat, "_IMPACTTYPE_SIMPLE", impactType == ImpactType.Simple);
        }
    }

    private enum ImpactIntensity { Max, Add }
    private ImpactIntensity CurrentImpactIntensity(Material _targetMat)
    {
        ImpactIntensity impactType = ImpactIntensity.Max;
        if (_targetMat.IsKeywordEnabled("_IMPACTINTENSITY_ADD"))
            impactType = ImpactIntensity.Add;

        return impactType;
    }

    protected void ShowImpactyIntensityEnum(Material _targetMat, MaterialEditor _editor)
    {
        ImpactIntensity impactIntensity = CurrentImpactIntensity(_targetMat);
        EditorGUI.BeginChangeCheck();
        impactIntensity = (ImpactIntensity)EditorGUILayout.EnumPopup(new GUIContent("Impact Intensity", "_ImpactIntensity - Shader Feature"), impactIntensity);
        if (EditorGUI.EndChangeCheck())
        {
            _editor.RegisterPropertyChangeUndo("Impact Intensity");
            SetKeyword(_targetMat, "_IMPACTINTENSITY_MAX", impactIntensity == ImpactIntensity.Max);
            SetKeyword(_targetMat, "_IMPACTINTENSITY_ADD", impactIntensity == ImpactIntensity.Add);
        }
    }

    private enum Coloring { Plain, Custom }
    private Coloring CurrentColoring(Material _targetMat)
    {
        Coloring coloring = Coloring.Custom;
        if (_targetMat.IsKeywordEnabled("_COLORING_PLAIN"))
            coloring = Coloring.Plain;

        return coloring;
    }

    protected void ShowColoringEnum(Material _targetMat, MaterialEditor _editor)
    {
        Coloring coloring = CurrentColoring(_targetMat);
        EditorGUI.BeginChangeCheck();
        coloring = (Coloring)EditorGUILayout.EnumPopup(new GUIContent("Coloring", "_Coloring - Shader Feature"), coloring);
        if (EditorGUI.EndChangeCheck())
        {
            _editor.RegisterPropertyChangeUndo("Coloring");
            SetKeyword(_targetMat, "_COLORING_PLAIN", coloring == Coloring.Plain);
            SetKeyword(_targetMat, "_COLORING_CUSTOM", coloring == Coloring.Custom);
        }
    }

    protected virtual void DoSpaceShieldArea(MaterialEditor _editor, MaterialProperty[] _properties)
    {
        GUILayout.Label("Rim", EditorStyles.boldLabel);
        ShowShaderProperty(_editor, _properties, "Color", "_RimColor", "");
        ShowShaderProperty(_editor, _properties, "Opacity", "_RimOpacity", "");
        ShowShaderProperty(_editor, _properties, "Power", "_RimPower", "");
    }
}
