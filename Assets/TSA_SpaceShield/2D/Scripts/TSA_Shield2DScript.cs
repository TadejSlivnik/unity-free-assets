﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TSA_Shield2DScript : MonoBehaviour
{
    public bool randomImpact;
    public float randomImpactCD = 0.1f;
    [Range(0.01f, 2)]
    public float impactForce = 0.5f;
    private float randomImpactTime;

    private List<TSA_Shield2DNode> nodes = new List<TSA_Shield2DNode>();
    private MeshRenderer meshRend;
    private const int maxPositions = 16;

    void Start()
    {
        meshRend = GetComponent<MeshRenderer>();
    }

    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            RaycastHit hit;
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            if (Physics.Raycast(ray, out hit))
                if (hit.collider.gameObject == gameObject)
                    Add(hit.point, impactForce);
        }

        if (randomImpact)
        {
            if (Time.time >= randomImpactTime)
            {
                randomImpactTime = Time.time + randomImpactCD;
                Add((Vector2)transform.position + Random.insideUnitCircle * 2, impactForce);
            }
        }

        for (int i = 0; i < nodes.Count; i++)
        {
            if (nodes[i].active)
            {
                if (nodes[i].IsAnimating)
                {
                    meshRend.material.SetVector("_Position" + i.ToString(),
                        new Vector4(nodes[i].pos.x, nodes[i].pos.y, nodes[i].intensity, Mathf.Clamp01(nodes[i].UpdateTime(Time.deltaTime)))
                        );
                }
                else
                {
                    nodes[i].active = false;
                    meshRend.material.SetVector("_Position" + i.ToString(), new Vector4(nodes[i].pos.x, nodes[i].pos.y, nodes[i].intensity, 0));
                }
            }
        }
    }

    public void Add(Vector2 _impactPosition, float _intensity)
    {
        TSA_Shield2DNode node = null;
        if (nodes.Count < maxPositions)
        {
            node = new TSA_Shield2DNode();
            nodes.Add(node);
        }
        else
            node = nodes.Find(x => x.active == false);

        if (node != null)
        {
            node.pos = (_impactPosition - (Vector2)transform.position) / transform.lossyScale.x;
            node.intensity = _intensity / transform.lossyScale.x;
            node.Reset();
        }
    }
}
