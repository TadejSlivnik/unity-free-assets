﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TSA_Shield2DNode
{
    public Vector2 pos = Vector2.zero;
    public float intensity;

    private float time;
    public float UpdateTime(float deltaT)
    {
        time += deltaT;
        return time;
    }

    public void Reset()
    {
        time = 0;
        active = true;
    }

    public bool active;
    public bool IsAnimating { get { return time < 1; } }
}
