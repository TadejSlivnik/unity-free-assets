﻿Shader "_TS/TSA/Shield/2D FX - Square, Circle"
{
	Properties 
	{
		[KeywordEnum(Normal, Radial)] _UVMode("UV Mode", Float) = 1
		[KeywordEnum(Wave, Simple)]_ImpactType("Impact Type", Float) = 0
		[KeywordEnum(Max, Add)]_ImpactIntensity("Impact Intensity", Float) = 0
		[KeywordEnum(Plain, Custom)]_Coloring("Coloring", Float) = 1
		// [Header(Texture)]
		_MainTex ("Main Texture", 2D) = "black" {}
		// [Header(Rim)]
		_RimColor("Rim Color", Color) = (1, 1, 1, 1)
		_RimPower("Rim Power", Range(0.5, 4)) = 2
		_RimOpacity("Rim Opacity", Range(0.0, 1.0)) = 1.0
	}
	SubShader
	{
		Blend SrcAlpha One
		Tags
		{ 
			"RenderType"="Transparent"
			"Queue"="Transparent"
		}
		ZWrite Off
		Lighting Off

		Pass 
		{

			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#pragma target 2.0

			#pragma multi_compile _UVMODE_NORMAL _UVMODE_RADIAL
			#pragma shader_feature _IMPACTTYPE_WAVE _IMPACTTYPE_SIMPLE
			#pragma shader_feature _IMPACTINTENSITY_MAX _IMPACTINTENSITY_ADD
			#pragma shader_feature _COLORING_PLAIN _COLORING_CUSTOM

			#include "UnityCG.cginc"

			sampler2D _MainTex;
			float4 _MainTex_ST;

			fixed4 _RimColor;
			float _RimPower;
			fixed _RimOpacity;
			
			float4 _Position0, _Position1, _Position2, _Position3;
			float4 _Position4, _Position5, _Position6, _Position7;
			float4 _Position8, _Position9, _Position10, _Position11;
			float4 _Position12, _Position13, _Position14, _Position15;

			struct appdata
			{
				float4 vertex : POSITION;
				fixed4 color : COLOR;
				float2 uv : TEXCOORD0;
			};

			struct v2f
			{
				float4 vertex : SV_POSITION;
				fixed2 pos : TEXCOORD0;
				float4 uv : TEXCOORD1;
			};

			v2f vert(appdata v)
			{
				v2f o;

				o.vertex = UnityObjectToClipPos(v.vertex);
				o.pos = v.vertex.xy;
				o.uv.xy = v.uv * 2 - 1;
				o.uv.zw = TRANSFORM_TEX(v.uv, _MainTex);

				return o;
			}
			
			fixed4 calcIntensity(float2 _vertexPos, float4 _impactPos) 
			{
				float impactRadius = _impactPos.w * _impactPos.z;
				float dist = length(_impactPos.xy - _vertexPos);

				#ifdef _IMPACTTYPE_WAVE
					float distFromRad = saturate(dist - impactRadius) / max(dist, saturate(_impactPos.w));
					fixed distFactor = (1 - sin(distFromRad)) * cos(distFromRad);
					distFactor *= distFactor;
					distFactor *= distFactor;
					distFactor *= distFactor;
					distFactor += impactRadius / (dist + _impactPos.z * 0.333) * 0.75;
				#else
					fixed distFactor = impactRadius / (dist + _impactPos.z * 0.2);
				#endif
				
				distFactor = saturate(distFactor * (1 - _impactPos.w));
				#ifdef _COLORING_CUSTOM
					return lerp(0, lerp(_RimColor, 1, distFactor * distFactor), distFactor);
				#else
					return distFactor * _RimColor;
				#endif
			}

			fixed4 frag(v2f i) : SV_Target
			{
				fixed4 intensity = 0;

				#ifdef _IMPACTINTENSITY_MAX
					intensity = max(intensity, calcIntensity(i.pos, _Position0));
					intensity = max(intensity, calcIntensity(i.pos, _Position1));
					intensity = max(intensity, calcIntensity(i.pos, _Position2));
					intensity = max(intensity, calcIntensity(i.pos, _Position3));
					intensity = max(intensity, calcIntensity(i.pos, _Position4));
					intensity = max(intensity, calcIntensity(i.pos, _Position5));
					intensity = max(intensity, calcIntensity(i.pos, _Position6));
					intensity = max(intensity, calcIntensity(i.pos, _Position7));
					intensity = max(intensity, calcIntensity(i.pos, _Position8));
					intensity = max(intensity, calcIntensity(i.pos, _Position9));
					intensity = max(intensity, calcIntensity(i.pos, _Position10));
					intensity = max(intensity, calcIntensity(i.pos, _Position11));
					intensity = max(intensity, calcIntensity(i.pos, _Position12));
					intensity = max(intensity, calcIntensity(i.pos, _Position13));
					intensity = max(intensity, calcIntensity(i.pos, _Position14));
					intensity = max(intensity, calcIntensity(i.pos, _Position15));
				#else
					intensity += calcIntensity(i.pos, _Position0);
					intensity += calcIntensity(i.pos, _Position1);
					intensity += calcIntensity(i.pos, _Position2);
					intensity += calcIntensity(i.pos, _Position3);
					intensity += calcIntensity(i.pos, _Position4);
					intensity += calcIntensity(i.pos, _Position5);
					intensity += calcIntensity(i.pos, _Position6);
					intensity += calcIntensity(i.pos, _Position7);
					intensity += calcIntensity(i.pos, _Position8);
					intensity += calcIntensity(i.pos, _Position9);
					intensity += calcIntensity(i.pos, _Position10);
					intensity += calcIntensity(i.pos, _Position11);
					intensity += calcIntensity(i.pos, _Position12);
					intensity += calcIntensity(i.pos, _Position13);
					intensity += calcIntensity(i.pos, _Position14);
					intensity += calcIntensity(i.pos, _Position15);
				#endif

				fixed2 radius = 1;
				#ifdef _UVMODE_NORMAL
					i.uv.xy = abs(i.uv.xy);
					radius.x = max(i.uv.x, i.uv.y);
				#else
					radius.x = sqrt(i.uv.x * i.uv.x + i.uv.y * i.uv.y);
				#endif

				#ifdef _UVMODE_RADIAL
					intensity *= saturate(ceil(1 - radius.x));
					radius.x *= saturate(ceil(1 - radius.x));
				#endif

				radius.x *= radius.x;
				radius.x *= radius.x;
				radius.x = pow(radius.x, _RimPower);
				
				// Rim
				radius.y = radius.x * 0.75;
				radius.y *= radius.y;
				#ifdef _COLORING_CUSTOM
					fixed4 rimCol = lerp(0, lerp(_RimColor, 1, radius.y), radius.x);
				#else
					fixed4 rimCol = _RimColor * radius.x;
				#endif
				rimCol *= _RimOpacity;

				#ifdef _IMPACTTYPE_WAVE
					rimCol += tex2D(_MainTex, i.uv.zw) * max(radius.x * (1 - radius.x), intensity * intensity);
				#else
					rimCol += tex2D(_MainTex, i.uv.zw) * max(radius.x * (1 - radius.x), intensity);
				#endif

				return intensity + rimCol;
			}
			ENDCG
		}
	}
    CustomEditor "PPS_Shield2DFXShaderGUI"
}
